<div class="container">
    <br>
    <br>  
    <!-- Form Infput Mahasiswa -->
    <div class="row">
      <form action="" method="post">
        <div>
          <label>Nama Mahasiswa : </label>
          <input type="text" name="nama_mhs" id="nama_mhs">
        </div>
        <div>
          <label>NIM : </label>
          <input type="text" name="nim" id="nim">
        </div>
        <div>
          <label>Jurusan : </label>
          <input type="text" name="jurusan" id="jurusan">
        </div>
        <div>
          <label>Fakultas : </label>
          <input type="text" name="fakultas" id="fakultas">
        </div>
        <div>
          <a class="btn btn-primary" id="btn-simpan">Simpan</a>
        </div>
      </form>
    </div>

    <br>
    <br>
    <!-- List Daftar Mahasiswa -->
    <div class="row">
      <table class="table">
        <thead>
          <tr>
            <td>No</td>
            <td>Nama Mahasiswa</td>
            <td>NIM</td>
            <td>Fakultas</td>
            <td>Jurusan</td>
            <td>Action</td>
          </tr>
        </thead>
        <tbody id="list-mahasiswa">

        </tbody>
      </table>
    </div>

    <!-- Modal Edit -->
    <div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                  <h4 class="modal-title" id="myModalLabel">Edit Data Mahasiswa</h4>
              </div>
              <form class="form-horizontal">
              <div class="modal-body">
                <div>
                  <label>Nama Mahasiswa : </label>
                  <input type="text" name="edit-nama_mhs" id="edit_nama_mhs">
                </div>
                <div>
                  <label>NIM : </label>
                  <input type="text" name="edit_nim" id="edit_nim">
                </div>
                <div>
                  <label>Jurusan : </label>
                  <input type="text" name="edit_jurusan" id="edit_jurusan">
                </div>
                <div>
                  <label>Fakultas : </label>
                  <input type="text" name="edit_fakultas" id="edit_fakultas">
                </div>
                <input type="hidden" name="kode" id="id-mahasiswa-edit" value="">                    
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <a href="javascript:;" class="btn_edit btn btn-primary" id="btn_edit">Edit</a>
              </div>
              </form>
          </div>
      </div>
    </div>

    <!-- MOdal Hapus -->
    <div class="modal fade" id="ModalHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                  <h4 class="modal-title" id="myModalLabel">Hapus Mahasiswa</h4>
              </div>
              <form class="form-horizontal">
              <div class="modal-body">
                                      
                      <input type="hidden" name="kode" id="id-mahasiswa-hps" value="">
                      <div class="alert alert-warning"><p>Apakah Anda yakin mau memhapus data mahasiswa ini?</p></div>
                                    
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                  <a href="javascript:;" class="btn_hapus btn btn-danger" id="btn_hapus">Hapus</a>
              </div>
              </form>
          </div>
      </div>
    </div>

</div>

<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>

<script type="text/javascript">
    $(document).ready(function(){
      list_mahasiswa();   //pemanggilan fungsi tampil mahasiswa.

      //fungsi tampil mahasiswa
      function list_mahasiswa(){
          $.ajax({
              type  : 'GET',
              url   : '<?php echo base_url()?>index.php/Mahasiswa/list_mahasiswa',
              async : false,
              dataType : 'json',
              success : function(data){
                  var html = '';
                  var i;
                  for (i = 0; i < data.length; i++) {
                      html += 
                      `
                      <tr class="trTable">
                        <td>`+(i+1)+`</td>
                        <td>`+data[i].nama+`</td>
                        <td>`+data[i].nim+`</td>
                        <td>`+data[i].jurusan+`</td>
                        <td>`+data[i].fakultas+`</td>
                        <td>
                          <a id="tab1-20933" class="btn btn-success item_edit" href="javascript:;" data="`+data[i].id_mahasiswa+`"> Edit</a>
                          <a id="tab1-20933" class="btn btn-danger item_hapus" href="javascript:;" data="`+data[i].id_mahasiswa+`"> Delete</a>
                        </td>
                      </tr>

                      `
                  }
                  
                  $('#list-mahasiswa').html(html);
              }
          }).fail(function(){
          });
      }

      //Simpan Mahasiswa
      $('#btn-simpan').on('click', function(){
          var nama_mhs=$('#nama_mhs').val();
          var nim=$('#nim').val();
          var jurusan=$('#jurusan').val();
          var fakultas=$('#fakultas').val();
          $.ajax({
              type : "POST",
              url  : "<?php echo base_url()?>index.php/Mahasiswa/insert_mahasiswa",
              dataType : "JSON",
              data : {nama_mhs:nama_mhs , nim:nim, jurusan:jurusan, fakultas:fakultas},
              success: function(data){
                  $('#nama_mhs').val("");
                  $('#nim').val("");
                  $('#jurusan').val("");
                  $('#fakultas').val("");
                  list_mahasiswa();
              }
          });
          return false;
      });

      //Tampil Form Edit
      $('#list-mahasiswa').on('click','.item_edit',function(){
        var id_mahasiswa = $(this).attr('data');
        $('#ModalEdit').modal('show');
        $.ajax({
          type : "GET",
          url : "<?php echo base_url() ?>index.php/Mahasiswa/detail_mahasiswa/"+id_mahasiswa,
          async : false,
          dataType : "JSON",
          success: function (data) {
            for (let i = 0; i < data.length; i++) {
              $('#id-mahasiswa-edit').val(id_mahasiswa);
              $('#edit_nama_mhs').val(data[i].nama); 
              $('#edit_nim').val(data[i].nim);
              $('#edit_jurusan').val(data[i].jurusan);
              $('#edit_fakultas').val(data[i].fakultas);
            }
          }
        });
      });

      //Edit Mahasiswa
      $('#btn_edit').on('click',function(){
        var id_mahasiswa=$('#id-mahasiswa-edit').val();
        console.log(id_mahasiswa);
        var nama_mhs=$('#edit_nama_mhs').val();
        var nim=$('#edit_nim').val();
        var jurusan=$('#edit_jurusan').val();
        var fakultas=$('#edit_fakultas').val();
        $.ajax({
            type : "POST",
            url  : "<?php echo base_url()?>index.php/Mahasiswa/update_mahasiswa/"+id_mahasiswa,
            dataType : "JSON",
            data : {nama_mhs:nama_mhs, nim:nim, jurusan:jurusan, fakultas:fakultas},
            success: function(data){
                $('#edit_nama_mhs').val("");
                $('#edit_nim').val("");
                $('#edit_jurusan').val("");
                $('#edit_fakultas').val("");
                $('#ModalEdit').modal('hide');
                list_mahasiswa();
            }
        });
        return false;
      });

      //Tampil tombol hapus
      $('#list-mahasiswa').on('click','.item_hapus',function(){
          var id=$(this).attr('data');
          $('#ModalHapus').modal('show');
          $('[name="kode"]').val(id);
      });

      //Hapus Mahasiswa
      $('#btn_hapus').on('click',function(){
          var id_mahasiswa=$('#id-mahasiswa-hps').val();
        console.log("kode");
          $.ajax({
          type : "POST",
          url  : "<?php echo base_url()?>index.php/Mahasiswa/delete_mahasiswa/"+id_mahasiswa,
          dataType : "JSON",
              data : {id_mahasiswa:id_mahasiswa},
              success: function(data){
                      $('#ModalHapus').modal('hide');
                      list_mahasiswa();
              }
          });
          return false;
      });

    });
</script>