<!DOCTYPE html>
<html>
<head>
	<title>Data Siswa</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
	<script type="text/javascript" src="<?php echo base_url('assets/jquery/jquery-3.3.1.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
</head>
<body style="margin: 20px;">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<b class="col-md-10">Masukan Data Siswa</b>
			<center><button data-toggle="modal" data-target="#addModal" class="btn btn-danger">Tambah Data Siswa</button></center>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>No.buku</th>
							<th>NamaBuku</th>
							<th>Angkatan</th>
							<th>Kelas</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody id="tbl_data">
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>

	<!-- Modal Tambah-->
	<div id="addModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Tambah Data</h4>
	      </div>
	      <div class="modal-body">
	        <form>
	        	<div class="form-group">
	        		<label for="nobuku">No.buku</label>
	        		<input type="text" name="nobuku" class="form-control"></input>
	        	</div>
	        	<div class="form-group">
	        		<label for="namabuku">Nama Buku</label>
	        		<input type="text" name="namabuku" class="form-control"></input>
	        	</div>
	        	<div class="form-group">
	        		<label for="angkatan">Angkatan</label>
	        		<input type="text" name="angkatan" class="form-control"></input>
	        	</div>
	        	<div class="form-group">
	        		<label for="kelas">Kelas</label>
	        		<input type="text" name="kelas" class="form-control"></input>
	        	</div>

	        </form>
	      </div>
	      <div class="modal-footer">
	       <button type="button" class="btn btn-success" id="btn_add_data">Simpan</button>
	       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>

	<!-- Modal Edit-->
	<div id="editModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Edit Data</h4>
	      </div>
	      <div class="modal-body">
	        <form>
	        	<div class="form-group">
	        		<label for="nobuku">Nobuku</label>
	        		<input type="text" name="nobuku_edit" class="form-control"></input>
	        	</div>
	        	<div class="form-group">
	        		<label for="namabuku">Nama Buku</label>
	        		<input type="text" name="namabuku_edit" class="form-control"></input>
	        	</div>
	        	<div class="form-group">
	        		<label for="angkatan">Angkatan</label>
	        		<input type="text" name="angkatan_edit" class="form-control"></input>
	        	</div>
	        	<div class="form-group">
	        		<label for="kelas">Kelas</label>
	        		<input type="text" name="kelas_edit" class="form-control"></input>
	        	</div>

	        </form>
	      </div>
	      <div class="modal-footer">
	       <button type="button" class="btn btn-success" id="btn_update_data">Update</button>
	       <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
	      </div>
	    </div>

  
	  </div>
	</div>
<button type="button" class="btn btn-success" onclick="goBack()">Go Back </button>
<script> 
	function goBack(){
		window.history.back();
	}
</script>
	
</button>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		tampil_data();
		//Menampilkan Data di tabel
		function tampil_data(){
			$.ajax({
				url: '<?php echo base_url(); ?>D_Index/ambilData',
				type: 'POST',
				dataType: 'json',
				success: function(response){
					console.log(response);
					var i;
					var no = 0;
					var html = "";
					for(i=0;i < response.length ; i++){
						no++;
						html = html + '<tr>'
									+ '<td>' + no  + '</td>'
									+ '<td>' + response[i].nobuku  + '</td>'
									+ '<td>' + response[i].namabuku  + '</td>'
									+ '<td>' + response[i].angkatan  + '</td>'
									+ '<td>' + response[i].kelas  + '</td>'
									+ '<td style="width: 16.66%;">' + '<span><button data-id="'+response[i].nobuku+'" class="btn btn-primary btn_edit">Edit</button><button style="margin-left: 5px;" data-id="'+response[i].nobuku+'" class="btn btn-danger btn_hapus">Hapus</button></span>'  + '</td>'
									+ '</tr>';
					}
					$("#tbl_data").html(html);
				}

			});
		}
		//Hapus Data dengan konfirmasi
		$("#tbl_data").on('click','.btn_hapus',function(){
			var nobuku = $(this).attr('data-id');
			var status = confirm('Yakin ingin menghapus?');
			if(status){
				$.ajax({
					url: '<?php echo base_url(); ?>D_Index/hapusData',
					type: 'POST',
					data: {nobuku:nobuku},
					success: function(response){
						tampil_data();
					}
				})
			}
		})
		//Menambahkan Data ke database
		$("#btn_add_data").on('click',function(){
			var nobuku = $('input[name="nobuku"]').val();
			var namabuku = $('input[name="namabuku"]').val();
			var angkatan = $('input[name="angkatan"]').val();
			var kelas = $('input[name="kelas"]').val();
			$.ajax({
				url: '<?php echo base_url(); ?>D_Index/tambahData',
				type: 'POST',
				data: {nobuku:nobuku,namabuku:namabuku,angkatan:angkatan,kelas:kelas},
				success: function(response){
					$('input[name="noinduk"]').val("");
					$('input[name="namabuku"]').val("");
					$('input[name="angkatan"]').val("");
					$('input[name="kelas"]').val("");
					$("#addModal").modal('hide');
					tampil_data();
				}
			})

		});
		//Memunculkan modal edit
		$("#tbl_data").on('click','.btn_edit',function(){
			var nobuku= $(this).attr('data-id');
			$.ajax({
				url: '<?php echo base_url(); ?>D_Index/ambilDataByNobuku',
				type: 'POST',
				data: {nobuku:nobuku},
				dataType: 'json',
				success: function(response){
					console.log(response);
					$("#editModal").modal('show');
					$('input[name="nobuku_edit"]').val(response[0].nobuku);
					$('input[name="namabuku_edit"]').val(response[0].namabuku);
					$('input[name="angkatan_edit"]').val(response[0].angkatan);
					$('input[name="kelas_edit"]').val(response[0].kelas);
				}
			})
		});

		//Meng-Update Data
		$("#btn_update_data").on('click',function(){
			var nobuku = $('input[name="nobuku_edit"]').val();
			var namabuku = $('input[name="namabuku_edit"]').val();
			var angkatan = $('input[name="angkatan_edit"]').val();
			var kelas = $('input[name="kelas_edit"]').val();
			$.ajax({
				url: '<?php echo base_url(); ?>D_Index/perbaruiData',
				type: 'POST',
				data: {nobuku:nobuku,namabuku:namabuku,angkatan:angkatan,kelas:kelas},
				success: function(response){
					$('input[name="nobuku_edit"]').val("");
					$('input[name="namabuku_edit"]').val("");
					$('input[name="angkatan_edit"]').val("");
					$('input[name="kelas_edit"]').val("");
					$("#editModal").modal('hide');
					tampil_data();
				}
			})

		});
	});
</script>