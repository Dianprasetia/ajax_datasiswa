<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class D_Index extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('A_Index');
	}

	public function index()
	{
		$this->load->view('B_Index');
	}

	function ambilData(){
		$data = $this->A_Index->getData();
		echo json_encode($data);
	}

	function ambilDataByNobuku(){
		$nobuku = $this->input->post('nobuku');
		$data = $this->A_Index->getDataByNobuku($nobuku);
		echo json_encode($data);
	}

	function hapusData(){
		$nobuku = $this->input->post('nobuku');
		$data = $this->A_Index->deleteData($nobuku);
		echo json_encode($data);
	}

	function tambahData(){
		$nobuku	= $this->input->post('nobuku');
		$namabuku 	= $this->input->post('namabuku');
		$angkatan	= $this->input->post('angkatan');
		$kelas		= $this->input->post('kelas');

		$data = ['nobuku' => $nobuku, 'namabuku' => $namabuku, 'angkatan' => $angkatan , 'kelas' => $kelas];
		$data = $this->A_Index->insertData($data);
		echo json_encode($data);
	}

	function perbaruiData(){
		$nobuku 	= $this->input->post('nobuku');
		$namabuku 	= $this->input->post('namabuku');
		$angkatan	= $this->input->post('angkatan');
		$kelas		= $this->input->post('kelas');

		$data = ['nobuku' => $nobuku, 'namabuku' => $namabuku, 'angkatan' => $angkatan , 'kelas' => $kelas];
		$data = $this->A_Index->updateData($nobuku,$data);
		
		echo json_encode($data);
	}
}
