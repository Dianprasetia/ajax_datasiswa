public function buku()
 {
  // Judul
  $data['ui_app_name'] = 'Perpus-Sys';
  $data['ui_title'] = 'Perpus-Sys - Data Buku';
 
  // CSS & JS
  $data['ui_css'] = array('custom/css/default.css');
  $data['ui_js'] = array();
 
  // Navigasi
  $data['ui_sidebar_nav'] = array(
   'Data|book' => array(
    'Data Buku|book|' .site_url('data/buku'),
    'Data Siswa|face|#',
   )
  );
  $data['ui_sidebar_active'] = 'Data|Data Buku';
  $data['ui_top_nav'] = array(
   'Buku|book|' .site_url('data/buku'),
   'Pegawai|face|#'
  );
  $data['ui_top_nav_active'] = 'Buku';
  $this->load->view('data/buku/list', $data);
 }