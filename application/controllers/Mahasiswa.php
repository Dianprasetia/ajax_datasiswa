<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mahasiswa extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model("M_Mahasiswa");
    }

    public function index()
    {
        $data["content"] = "home"; 
        $this->load->view('layout/wrapper', $data);
    }

    public function list_mahasiswa()
    {
        $mahasiswa = $this->M_mahasiswa->get_mahasiswa();
        echo json_encode($mahasiswa); //mengencode data menjadi json untuk dikirim ke ajax
    }

    public function insert_mahasiswa()
    {
        $nama = $this->input->post('nama_mhs');
        $nim = $this->input->post('nim');
        $jurusan = $this->input->post('jurusan');
        $fakultas = $this->input->post('fakultas');

        $data = array(
                'nama' => $nama,
                'nim' => $nim,
                'jurusan' => $jurusan,
                'fakultas' => $fakultas
            );
        
        $mahasiswa = $this->M_mahasiswa->insert_mhs($data);
        echo json_encode($mahasiswa); //mengencode data menjadi json untuk dikirim ke ajax
    }

    public function detail_mahasiswa($id)
    {
        $where = array("id_mahasiswa" => $id);
        $mahasiswa = $this->M_mahasiswa->detail($where);
        echo json_encode($mahasiswa); //mengencode data menjadi json untuk dikirim ke ajax
    }

    public function update_mahasiswa($id)
    {
        $where = array('id_mahasiswa' => $id, );

        $nama = $this->input->post('nama_mhs');
        $nim = $this->input->post('nim');
        $jurusan = $this->input->post('jurusan');
        $fakultas = $this->input->post('fakultas');

        $data = array(
                'nama' => $nama,
                'nim' => $nim,
                'jurusan' => $jurusan,
                'fakultas' => $fakultas
            );
        
        $mahasiswa = $this->M_mahasiswa->update($where, $data);
        echo json_encode($mahasiswa); //mengencode data menjadi json untuk dikirim ke ajax   
    }

    public function delete_mahasiswa($id)
    {
        $where = array("id_mahasiswa" => $id);
        $mahasiswa = $this->M_mahasiswa->delete($where);
        echo json_encode($mahasiswa); //mengencode data menjadi json untuk dikirim ke ajax
    }
    
}
?>