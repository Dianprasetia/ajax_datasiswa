<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('pegawaiModel'); // Load PegawaiModel ke controller ini
	}

	public function index(){
		$data['model'] = $this->pegawaiModel->view();

		$this->load->view('pegawai/index', $data);
	}

	public function simpan(){
		if($this->pegawaiModel->validation("save")){ // Jika validasi sukses atau hasil validasi adalah true
			$this->pegawaiModel->save(); // Panggil fungsi save() yang ada di PegawaiModel.php

			// Load ulang view.php agar data yang baru bisa muncul di tabel pada view.php
			$html = $this->load->view('pegawai/view', array('model'=>$this->pegawaiModel->view()), true);

			$callback = array(
				'status'=>'sukses',
				'pesan'=>'Data berhasil disimpan',
				'html'=>$html
			);
		}else{
			$callback = array(
				'status'=>'gagal',
				'pesan'=>validation_errors()
			);
		}

		echo json_encode($callback);
	}

	public function ubah($id){
		if($this->pegawaiModel->validation("update")){ // Jika validasi sukses atau hasil validasi adalah true
			$this->pegawaiModel->edit($id); // Panggil fungsi edit() yang ada di PegawaiModel.php

			// Load ulang view.php agar data yang baru bisa muncul di tabel pada view.php
			$html = $this->load->view('pegawai/view', array('model'=>$this->pegawaiModel->view()), true);

			$callback = array(
				'status'=>'sukses',
				'pesan'=>'Data berhasil diubah',
				'html'=>$html
			);
		}else{
			$callback = array(
				'status'=>'gagal',
				'pesan'=>validation_errors()
			);
		}

		echo json_encode($callback);
	}

	public function hapus($id){
		$this->pegawaiModel->delete($id); // Panggil fungsi delete() yang ada di PegawaiModel.php

		// Load ulang view.php agar data yang baru bisa muncul di tabel pada view.php
		$html = $this->load->view('pegawai/view', array('model'=>$this->pegawaiModel->view()), true);
		
		$callback = array(
			'status'=>'sukses',
			'pesan'=>'Data berhasil dihapus',
			'html'=>$html
		);

		echo json_encode($callback);
	}
}
