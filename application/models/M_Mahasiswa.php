<?php defined('BASEPATH') OR exit('No direct script access allowed');
    
    class M_mahasiswa extends CI_Model{
        
        public function get_mahasiswa()
        {
            $this->db->from("mahasiswa");
            $q = $this->db->get();
            return $q->result();
        }

        public function insert_mhs($data)
        {
            $this->db->insert("mahasiswa", $data);
        }

        public function detail($where)
        {
            $this->db->from("mahasiswa");
            $this->db->where($where);
            $query = $this->db->get();
            return $query->result();
        }

        public function update($where, $data)
        {
            $this->db->where($where);
            $this->db->update("mahasiswa", $data);
        }

        public function delete($where)
        {
            $this->db->where($where);
            $this->db->delete('mahasiswa');
        }

    }
?>