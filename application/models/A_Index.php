<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class A_Index extends CI_Model {

	function getData(){
		return $this->db->get('tb_mapel')->result();
	}

	function getDataByNobuku($nobuku){
		$this->db->where('nobuku',$nobuku);
		return $this->db->get('tb_mapel')->result();
	}

	function deleteData($nobuku){
		$this->db->where('nobuku',$nobuku);
		$this->db->delete('tb_mapel');
	}

	function insertData($data){
		$this->db->insert('tb_mapel',$data);
	}

	function updateData($nobuku,$data){
		$this->db->where('nobuku,$nobuku');
		$this->db->update('tb_mapel',$data);
	}
}
