<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PegawaiModel extends CI_Model {
	// Fungsi untuk menampilkan semua data siswa
	public function view(){
		return $this->db->get('pegawai')->result();
	}

	// Fungsi untuk validasi form tambah dan ubah
	public function validation($mode){
		$this->load->library('form_validation'); // Load library form_validation untuk proses validasinya

		// Tambahkan if apakah $mode save atau update
		// Karena ketika update, NIK tidak harus divalidasi
		// Jadi NIK di validasi hanya ketika menambah data pegawai saja
		if($mode == "save")
			$this->form_validation->set_rules('input_nik', 'NIK', 'required|max_length[20]');

		$this->form_validation->set_rules('input_nama', 'Nama', 'required|max_length[50]');
		$this->form_validation->set_rules('input_jeniskelamin', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('input_telp', 'telp', 'required|numeric|max_length[15]');
		$this->form_validation->set_rules('input_alamat', 'Alamat', 'required');

		if($this->form_validation->run()) // Jika validasi benar
			return true; // Maka kembalikan hasilnya dengan TRUE
		else // Jika ada data yang tidak sesuai validasi
			return false; // Maka kembalikan hasilnya dengan FALSE
	}

	// Fungsi untuk melakukan simpan data ke tabel pegawai
	public function save(){
		$data = array(
			"nik" => $this->input->post('input_nik'),
			"nama" => $this->input->post('input_nama'),
			"jenis_kelamin" => $this->input->post('input_jeniskelamin'),
			"telp" => $this->input->post('input_telp'),
			"alamat" => $this->input->post('input_alamat')
		);

		$this->db->insert('pegawai', $data); // Untuk mengeksekusi perintah insert data
	}

	// Fungsi untuk melakukan ubah data pegawai berdasarkan ID Pegawai
	public function edit($id){
		$data = array(
			"nik" => $this->input->post('input_nik'),
			"nama" => $this->input->post('input_nama'),
			"jenis_kelamin" => $this->input->post('input_jeniskelamin'),
			"telp" => $this->input->post('input_telp'),
			"alamat" => $this->input->post('input_alamat')
		);

		$this->db->where('id', $id);
		$this->db->update('pegawai', $data); // Untuk mengeksekusi perintah update data
	}

	// Fungsi untuk melakukan menghapus data pegawai berdasarkan ID Pegawai
	public function delete($id){
		$this->db->where('id', $id);
		$this->db->delete('pegawai'); // Untuk mengeksekusi perintah delete data
	}
}
